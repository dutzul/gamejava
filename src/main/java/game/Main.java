package game;

import java.awt.*;
import java.util.ArrayList;

public class Main {

    static int width=900;
    static int height=600;

    static void addTrajectory(World myWorld){
        ArrayList<Point> path=new ArrayList<Point>();
        for(int i=width/100;i<width;i+=width/100){
            path.add(new Point(height/2,i));
            System.out.println(path.get(path.size()-1));
        }
        Trajectory myTrajectory=new Trajectory(path);
    }

    static void addTower(World myWorld){
        for(int i=width/5;i<width;i+=width/5){
            myWorld.myTowers.add(new Tower(i,height/4,1,height/2));
        }
    }

    public static void main(String[] args){
        World myWorld = new World();
        addTrajectory(myWorld);


    }
}
